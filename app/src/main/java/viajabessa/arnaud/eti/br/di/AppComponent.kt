package viajabessa.arnaud.eti.br.di

import dagger.Component
import viajabessa.arnaud.eti.br.viewmodel.TravelDetailViewModel
import viajabessa.arnaud.eti.br.viewmodel.TravelListViewModel
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class])
interface AppComponent {
    fun inject(i: TravelListViewModel)
    fun inject(i: TravelDetailViewModel)
}