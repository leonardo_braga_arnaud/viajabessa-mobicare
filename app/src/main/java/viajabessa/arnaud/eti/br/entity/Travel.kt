package viajabessa.arnaud.eti.br.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Travel(
    @PrimaryKey
    val id: Long,
    val title: String,
    val value: Double,
    val image: String,
    val description: String
) {
    var sold: Boolean = false
    var type: Int = 0

    companion object {
        const val ID = "Travel_ID"
        const val BRAZIL_TYPE = 1
        const val WORLD_TYPE = 2
    }
}