package viajabessa.arnaud.eti.br.helpers

class AppMessenger(val id: Int, val message: String) {

    companion object {

        const val SNACKBAR_ERROR = 100
        const val DIALOG_ERROR = 200
        const val FORM_ERROR = 300
        const val TOAST_ERROR = 300
        const val TRY_AGAIN_SNACKBAR = 400


    }
}