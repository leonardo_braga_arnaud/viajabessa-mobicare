package viajabessa.arnaud.eti.br.repository.remote.communication

import viajabessa.arnaud.eti.br.entity.Travel

class ApiaryListResponse(val data: List<Travel>)