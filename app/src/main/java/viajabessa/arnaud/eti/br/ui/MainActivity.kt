package viajabessa.arnaud.eti.br.ui

import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import viajabessa.arnaud.eti.br.R
import viajabessa.arnaud.eti.br.helpers.createIntent
import viajabessa.arnaud.eti.br.ui.travels.TravelListActivity

class MainActivity : AppCompatActivity() {

    private val handler = Handler()
    private val runnable = Runnable {
        startActivity(createIntent<TravelListActivity>())
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        handler.postDelayed(runnable, 3000)
    }

    override fun onDestroy() {
        super.onDestroy()

        handler.removeCallbacks(runnable)
    }
}
