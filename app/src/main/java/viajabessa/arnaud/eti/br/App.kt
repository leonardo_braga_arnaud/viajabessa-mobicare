package viajabessa.arnaud.eti.br

import android.app.Application
import viajabessa.arnaud.eti.br.di.AppComponent
import viajabessa.arnaud.eti.br.di.AppModule
import viajabessa.arnaud.eti.br.di.DaggerAppComponent

class App : Application() {

    lateinit var appComponent: AppComponent

    companion object {
        lateinit var instance: App
    }

    override fun onCreate() {
        super.onCreate()

        instance = this

        // Dagger
        appComponent = DaggerAppComponent.builder()
            .appModule(AppModule(this))
            .build()
    }
}