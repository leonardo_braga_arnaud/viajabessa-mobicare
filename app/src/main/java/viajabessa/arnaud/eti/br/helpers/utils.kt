package viajabessa.arnaud.eti.br.helpers

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.ColorMatrix
import android.graphics.ColorMatrixColorFilter
import android.widget.ImageView
import androidx.core.os.bundleOf

inline fun <reified T : Activity> Context.createIntent(vararg extras: Pair<String, Any?>) =
    Intent(this, T::class.java).apply {
        putExtras(bundleOf(*extras))
    }

fun ImageView.setGrayScale(b: Boolean) {
    if (b) {
        val matrix = ColorMatrix()
        matrix.setSaturation(0f)
        val cf = ColorMatrixColorFilter(matrix)
        this.colorFilter = cf
    } else {
        this.colorFilter = null
    }

}