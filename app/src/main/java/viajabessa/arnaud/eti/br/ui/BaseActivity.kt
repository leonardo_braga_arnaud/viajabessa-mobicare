package viajabessa.arnaud.eti.br.ui

import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.google.android.material.snackbar.Snackbar
import viajabessa.arnaud.eti.br.R
import viajabessa.arnaud.eti.br.helpers.AppMessenger
import viajabessa.arnaud.eti.br.viewmodel.BaseViewModel

abstract class BaseActivity : AppCompatActivity() {

    abstract val viewModel: BaseViewModel

    private var messageObserver = Observer<AppMessenger> { appMessenger ->
        appMessenger?.let {
            val rootView = window.decorView.rootView
            when (appMessenger.id) {
                AppMessenger.SNACKBAR_ERROR ->
                    Snackbar.make(rootView, appMessenger.message, Snackbar.LENGTH_LONG).show()
                AppMessenger.TRY_AGAIN_SNACKBAR ->
                    Snackbar.make(rootView, appMessenger.message, Snackbar.LENGTH_INDEFINITE)
                        .setAction(R.string.try_again) {
                            viewModel.tryAgain.postValue(true)
                        }.show()
                AppMessenger.TOAST_ERROR ->
                    Toast.makeText(this@BaseActivity, appMessenger.message, Toast.LENGTH_LONG).show()
                else -> Log.i("Message", appMessenger.message)
            }
        }
    }

    private var loadingObserver = Observer<Boolean> {
        val progressBar = findViewById<ProgressBar>(R.id.loadingProgressBar)
        if (progressBar != null) {
            progressBar.visibility = if (it) View.VISIBLE else View.GONE
            val rootView = window.decorView.rootView
            enableDisableView(rootView, !it)
        }
    }

    override fun onResume() {
        super.onResume()

        try {
            viewModel.message.value = null
            viewModel.message.observe(this, messageObserver)
            viewModel.loading.observe(this, loadingObserver)
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    override fun onPause() {
        super.onPause()
        try {
            viewModel.message.removeObserver(messageObserver)
            viewModel.loading.removeObserver(loadingObserver)
        } catch (ignored: Exception) {
        }

    }

    private fun enableDisableView(rootView: View, enabled: Boolean) {
        rootView.isEnabled = enabled

        try {
            val viewGroup = rootView as ViewGroup
            for (i in 0 until viewGroup.childCount)
                enableDisableView(viewGroup.getChildAt(i), enabled)
        } catch (ignored: Exception) {
        }

    }

}