package viajabessa.arnaud.eti.br.repository.db.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import viajabessa.arnaud.eti.br.entity.Travel

@Dao
interface TravelDao {

    @Query("SELECT * FROM Travel WHERE type = ${Travel.BRAZIL_TYPE}")
    fun getBrazilTravels(): LiveData<List<Travel>>

    @Query("SELECT * FROM Travel WHERE type = ${Travel.WORLD_TYPE}")
    fun getWorldTravels(): LiveData<List<Travel>>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(items: List<Travel>)

    @Query("SELECT * FROM Travel WHERE id = :id")
    fun getTravel(id: Long): LiveData<Travel>

    @Query("UPDATE Travel SET sold = :b WHERE id = :id")
    fun updateSold(id: Long, b: Boolean)

    @Query("SELECT count(*) FROM Travel")
    fun getTotalRegisteredTravels(): Int

}
