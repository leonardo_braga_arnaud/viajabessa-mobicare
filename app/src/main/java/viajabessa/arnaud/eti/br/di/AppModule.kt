package viajabessa.arnaud.eti.br.di

import androidx.room.Room
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import viajabessa.arnaud.eti.br.App
import viajabessa.arnaud.eti.br.R
import viajabessa.arnaud.eti.br.repository.db.AppDatabase
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
@SuppressWarnings("unused")
class AppModule(private val application: App) {

    @Provides
    @Singleton
    fun provideRetrofit(): Retrofit {
        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY
        val client = OkHttpClient.Builder()
            .connectTimeout(application.resources.getInteger(R.integer.connect_timeout).toLong(), TimeUnit.SECONDS)
            .writeTimeout(application.resources.getInteger(R.integer.write_timeout).toLong(), TimeUnit.SECONDS)
            .readTimeout(application.resources.getInteger(R.integer.read_timeout).toLong(), TimeUnit.SECONDS)
            .addInterceptor(logging)
            .addInterceptor(HeaderInterceptor())
            .build()
        return Retrofit.Builder()
            .baseUrl(application.getString(R.string.base_url))
            .client(client)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    @Singleton
    @Provides
    fun provideAppDatabase(): AppDatabase {
        val dbName = application.resources.getString(R.string.db_name)
        return Room.databaseBuilder(application, AppDatabase::class.java, dbName)
            .fallbackToDestructiveMigration()
            .build()
    }

}