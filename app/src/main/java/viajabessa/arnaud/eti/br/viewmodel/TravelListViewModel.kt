package viajabessa.arnaud.eti.br.viewmodel

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import viajabessa.arnaud.eti.br.App
import viajabessa.arnaud.eti.br.R
import viajabessa.arnaud.eti.br.entity.Travel
import viajabessa.arnaud.eti.br.helpers.AppMessenger
import viajabessa.arnaud.eti.br.repository.db.AppDatabase
import viajabessa.arnaud.eti.br.repository.remote.communication.ApiaryListResponse
import viajabessa.arnaud.eti.br.repository.remote.service.ApiaryService
import javax.inject.Inject

class TravelListViewModel(val app: App) : BaseViewModel(app) {

    lateinit var db: AppDatabase @Inject set
    lateinit var retrofit: Retrofit @Inject set

    val brazilTravels: LiveData<List<Travel>>
    val worldTravels: LiveData<List<Travel>>

    init {
        app.appComponent.inject(this)
        brazilTravels = db.travelDao().getBrazilTravels()
        worldTravels = db.travelDao().getWorldTravels()
        loadTravels()
    }

    fun loadTravels() {
        loading.postValue(true)
        val api = retrofit.create(ApiaryService::class.java)
        api.getBrazilList().enqueue(getTravelsCallback(Travel.BRAZIL_TYPE))
        api.getWorldList().enqueue(getTravelsCallback(Travel.WORLD_TYPE))
    }

    private fun getTravelsCallback(type: Int): Callback<ApiaryListResponse> {
        return object : Callback<ApiaryListResponse> {
            override fun onResponse(
                call: Call<ApiaryListResponse>,
                response: Response<ApiaryListResponse>
            ) {
                loading.postValue(false)
                if (response.body() != null) {
                    val apiaryListResponse = response.body()
                    GlobalScope.launch {
                        apiaryListResponse?.let { apiaryResponse ->
                            apiaryResponse.data.let { travelList ->
                                travelList.map { travel -> travel.type = type }
                                db.travelDao().insert(apiaryListResponse.data)
                            }
                        }
                    }
                } else {
                    sendErroSyncTravelsMessage()
                }
            }

            override fun onFailure(
                call: Call<ApiaryListResponse>,
                t: Throwable
            ) {
                loading.postValue(false)
                sendErroSyncTravelsMessage()
            }
        }
    }

    private fun sendErroSyncTravelsMessage() {
        GlobalScope.launch {
            val total = db.travelDao().getTotalRegisteredTravels()
            val msg = app.getString(R.string.error_sync_travels)
            val msgId = if (total > 0) AppMessenger.SNACKBAR_ERROR else AppMessenger.TRY_AGAIN_SNACKBAR
            message.postValue(AppMessenger(msgId, msg))
        }
    }

    class Factory(private val app: Application) : ViewModelProvider.NewInstanceFactory() {
        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            @Suppress("UNCHECKED_CAST") return TravelListViewModel(app as App) as T
        }
    }
}