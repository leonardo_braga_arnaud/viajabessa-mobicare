package viajabessa.arnaud.eti.br.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import viajabessa.arnaud.eti.br.helpers.AppMessenger

abstract class BaseViewModel(baseApp: Application) : AndroidViewModel(baseApp) {

    var message = MutableLiveData<AppMessenger>()
    var loading = MutableLiveData<Boolean>()
    var tryAgain = MutableLiveData<Boolean>()

}