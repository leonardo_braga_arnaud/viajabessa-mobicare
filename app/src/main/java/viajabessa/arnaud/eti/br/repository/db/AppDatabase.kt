package viajabessa.arnaud.eti.br.repository.db

import androidx.room.Database
import androidx.room.RoomDatabase
import viajabessa.arnaud.eti.br.entity.Travel
import viajabessa.arnaud.eti.br.repository.db.dao.TravelDao

@Database(
    entities = [
        Travel::class
    ],
    version = 2,
    exportSchema = false
)
abstract class AppDatabase : RoomDatabase() {
    abstract fun travelDao(): TravelDao
}
