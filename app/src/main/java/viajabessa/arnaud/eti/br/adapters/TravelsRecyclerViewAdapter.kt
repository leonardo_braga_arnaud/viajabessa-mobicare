package viajabessa.arnaud.eti.br.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.item_travel_list.view.*
import viajabessa.arnaud.eti.br.R
import viajabessa.arnaud.eti.br.entity.Travel
import viajabessa.arnaud.eti.br.helpers.setGrayScale
import java.text.NumberFormat

class TravelsRecyclerViewAdapter(private var items: List<Travel>, private val listener: TravelClickListener) :
    RecyclerView.Adapter<TravelsRecyclerViewAdapter.ViewHolder>() {

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindView(items[position])
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.item_travel_list, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onViewRecycled(holder: ViewHolder) {
        super.onViewRecycled(holder)

        Glide.with(holder.itemView.context).clear(holder.itemView.imageView)
    }

    fun update(travels: List<Travel>) {
        items = travels
        notifyDataSetChanged()
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindView(travel: Travel) {
            itemView.titleTextView.text = travel.title
            itemView.valueTextView.text = NumberFormat.getCurrencyInstance().format(travel.value)
            Glide.with(itemView.context)
                .load(travel.image)
                .into(itemView.imageView)
            itemView.setOnClickListener { listener.onClick(travel.id) }
            itemView.soldTextView.visibility = if (travel.sold) View.VISIBLE else View.GONE
            itemView.imageView.setGrayScale(travel.sold)
        }
    }

    interface TravelClickListener {
        fun onClick(travelId: Long)
    }
}