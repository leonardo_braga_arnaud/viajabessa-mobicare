package viajabessa.arnaud.eti.br.ui.travels

import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_travel_detail.*
import kotlinx.android.synthetic.main.content_travel_detail.*
import viajabessa.arnaud.eti.br.R
import viajabessa.arnaud.eti.br.entity.Travel
import viajabessa.arnaud.eti.br.helpers.setGrayScale
import viajabessa.arnaud.eti.br.ui.BaseActivity
import viajabessa.arnaud.eti.br.viewmodel.TravelDetailViewModel
import java.text.NumberFormat

class TravelDetailActivity : BaseActivity() {

    override lateinit var viewModel: TravelDetailViewModel
    private var travelId: Long = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_travel_detail)

        val bundle = savedInstanceState ?: intent.extras
        travelId = bundle?.getLong(Travel.ID) ?: 0

        viewModel = ViewModelProviders
            .of(this, TravelDetailViewModel.Factory(application, travelId))
            .get(TravelDetailViewModel::class.java)

        setSupportActionBar(toolbar)

        buyFab.setOnClickListener {
            viewModel.buyTravel(travelId)
        }

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        setupObservers()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun setupObservers() {
        viewModel.travel.observe(this, Observer { travel -> setupLayout(travel) })
    }

    private fun setupLayout(travel: Travel) {
        titleTextView.text = travel.title
        valueTextView.text = NumberFormat.getCurrencyInstance().format(travel.value)
        descriptionTextView.text = travel.description
        Glide.with(this)
            .load(travel.image)
            .into(imageView)
        if (travel.sold) {
            buyFab.hide()
            soldTextView.visibility = View.VISIBLE
        } else {
            buyFab.show()
            soldTextView.visibility = View.GONE
        }
        imageView.setGrayScale(travel.sold)
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)

        outState?.putLong(Travel.ID, travelId)
    }
}
