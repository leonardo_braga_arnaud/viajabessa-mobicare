package viajabessa.arnaud.eti.br.ui.travels

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import kotlinx.android.synthetic.main.activity_travel_list.*
import kotlinx.android.synthetic.main.content_travel_list.*
import viajabessa.arnaud.eti.br.R
import viajabessa.arnaud.eti.br.adapters.TravelsRecyclerViewAdapter
import viajabessa.arnaud.eti.br.entity.Travel
import viajabessa.arnaud.eti.br.helpers.createIntent
import viajabessa.arnaud.eti.br.ui.BaseActivity
import viajabessa.arnaud.eti.br.viewmodel.TravelListViewModel

class TravelListActivity : BaseActivity() {

    override lateinit var viewModel: TravelListViewModel

    private val brazilTravelsObserver = Observer<List<Travel>> { travels ->
        if (brazilRecyclerView.adapter == null) {
            brazilRecyclerView.adapter = TravelsRecyclerViewAdapter(travels, travelClickListener)
        } else {
            val adapter = brazilRecyclerView.adapter as TravelsRecyclerViewAdapter
            adapter.update(travels)
        }
    }

    private val worldTravelsObserver = Observer<List<Travel>> { travels ->
        if (worldRecyclerView.adapter == null) {
            worldRecyclerView.adapter = TravelsRecyclerViewAdapter(travels, travelClickListener)
        } else {
            val adapter = worldRecyclerView.adapter as TravelsRecyclerViewAdapter
            adapter.update(travels)
        }
    }

    private val tryAgainObserver = Observer<Boolean> { b -> if (b) viewModel.loadTravels() }

    private val travelClickListener = object : TravelsRecyclerViewAdapter.TravelClickListener {
        override fun onClick(travelId: Long) {
            startActivity(createIntent<TravelDetailActivity>(Pair(Travel.ID, travelId)))
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_travel_list)
        viewModel = ViewModelProviders
            .of(this, TravelListViewModel.Factory(application))
            .get(TravelListViewModel::class.java)
        setSupportActionBar(toolbar)
        setupObservers()
    }

    override fun onDestroy() {
        super.onDestroy()

        viewModel.brazilTravels.removeObserver(brazilTravelsObserver)
        viewModel.worldTravels.removeObserver(worldTravelsObserver)
        viewModel.tryAgain.removeObserver(tryAgainObserver)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_travel_list, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.action_about -> openLinkedinProfile()
            else -> return false
        }
        return true
    }

    private fun openLinkedinProfile() {
        val i = Intent(Intent.ACTION_VIEW)
        i.data = Uri.parse("https://www.linkedin.com/in/leonardoandroidjava/")
        startActivity(i)
    }

    private fun setupObservers() {
        viewModel.brazilTravels.observeForever(brazilTravelsObserver)
        viewModel.worldTravels.observeForever(worldTravelsObserver)
        viewModel.tryAgain.observeForever(tryAgainObserver)
    }

}
