package viajabessa.arnaud.eti.br.di

import android.os.Build
import okhttp3.Interceptor
import okhttp3.Response

class HeaderInterceptor : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response = chain.run {
        proceed(
            request()
                .newBuilder()
                .addHeader("manufacturer", Build.MANUFACTURER)
                .addHeader("model", Build.MODEL)
                .addHeader("version", Build.VERSION.SDK_INT.toString())
                .addHeader("versionRelease", Build.VERSION.RELEASE)
                .build()
        )
    }
}
