package viajabessa.arnaud.eti.br.viewmodel

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import retrofit2.Retrofit
import viajabessa.arnaud.eti.br.App
import viajabessa.arnaud.eti.br.entity.Travel
import viajabessa.arnaud.eti.br.repository.db.AppDatabase
import javax.inject.Inject

class TravelDetailViewModel(val app: App, val id: Long) : BaseViewModel(app) {

    lateinit var db: AppDatabase @Inject set
    lateinit var retrofit: Retrofit @Inject set

    val travel: LiveData<Travel>

    init {
        app.appComponent.inject(this)
        travel = db.travelDao().getTravel(id)
    }

    fun buyTravel(travelId: Long) {
        GlobalScope.launch {
            db.travelDao().updateSold(travelId, true)
        }
    }

    class Factory(private val app: Application, private val id: Long) : ViewModelProvider.NewInstanceFactory() {
        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            @Suppress("UNCHECKED_CAST") return TravelDetailViewModel(app as App, id) as T
        }
    }
}