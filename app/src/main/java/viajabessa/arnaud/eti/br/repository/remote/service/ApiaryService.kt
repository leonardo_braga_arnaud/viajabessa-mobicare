package viajabessa.arnaud.eti.br.repository.remote.service

import retrofit2.Call
import retrofit2.http.GET
import viajabessa.arnaud.eti.br.repository.remote.communication.ApiaryListResponse

interface ApiaryService {

    @GET(WORLD_TRAVELS_URL)
    fun getWorldList(): Call<ApiaryListResponse>

    @GET(BRAZIL_TRAVELS_URL)
    fun getBrazilList(): Call<ApiaryListResponse>

    companion object {

        const val WORLD_TRAVELS_URL = "travel_packages/1"
        const val BRAZIL_TRAVELS_URL = "travel_packages/2"
    }

}